# How to rotate OpenStack images to achieve cloud users use less vulnerable code bases?


Read more information about the concept [here](https://docs.google.com/document/d/1kyr7GMUv0gioo38skkrsFJ-KXkjkfSjEELVkojSWF7k/view).


This is v2 codebase which is not deployed in G2 OpenStack cloud yet.
Original v1 codebase was simplified and rewritten from shell / python into ansible.


Although we tend to prefer in G2 OpenStack Terraform IaC/GitOps toolset over ansible, here it can be dangerous to use Terraform directly.

Repository cloud-entities keeps definition of images and image-rotation ansible makes sure it guarantees images are present and fresh.

It is necessary to say that technology for image rotation is slightly controversal (regardless of the used implementation).

On one side most of cloud users benefit from having images updated (say every 3-6 months) on the other "dark" side experienced people often
 mention necessity to use image ID rather than image name in Terraform IaC code as images being rotated may result in infrastructure rebuild
 of their infrastructures. Sometimes users do not want to rebuild owned infrastructure just because there is new version of underlaying Operating System. In the Terraform case user may either use image ID instead of image name or get current image name based on used image ID (TF data source) in order to keep infrastructure on outdated OS versions.

To fight this controversy we ended up performing image rotation much less frequently than we initially thought (3-6 months).

