# explicit-exports

## [OpenStack image rotation](image-rotation/README.md)

[How to safely rotate OpenStack cloud images.](image-rotation/README.md)
